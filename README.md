-----------------------------------------------------2021/03/31開始Java Spring Boot的學習 -----------------------------------------------------
參考https://www.itread01.com/content/1548629288.html，用eclipse建立Spring Boot專案連接到MySQL
該網站有一些地方需做修正！
    1. damain => domain
    2. 在dao檔的部分，需加上@Service
    3. 承2，將dao檔外，其他有@Service跟@Autowired地方刪掉，也能幫助專案執行成功
    
不過根據 https://www.itread01.com/content/1548629288.html 的方法，並做一些修正，仍然不能順利實現簡單的save(insert資料進入表格中)功能，
開啟127.0.0.1:8080會出現error的相關訊息，目前推測是我的開發環境，無法透過JDBC連接到MySQL，因此@Autowired無法正常執行！目前是使用手動
coding的方法，將原本@Autowired應該要產生的東西，手動寫出來，已能成功執行功能，並有在資料庫的表格中，看見插入的資料！

只不過仍須知道@Autowired無法正常使用的原因！並且最好一步一步學習Spring Boot，不然會浪費很多時間在錯誤的方向！


----------------------------------------------------------2021/04/08更新----------------------------------------------------------
1.前次@Autowired無法執行成功的原因，推測是@Service沒寫好，導致程式執行失敗！
2.本次學習，學會了更多使用Spring Boot專案操作MySQL的方法，目前已能進行新增、查詢、刪除資料的操作，並有做一些除錯機制！
    操作方法例子(輸入url)：
    http://127.0.0.1:8080/getUser1?email=jack@gmail.com&name=Elsa
    http://127.0.0.1:8080/adduser?id=85462321&email=gogo51@gmail.com&name=Teddy
    http://127.0.0.1:8080/getUserName?name=Teddy
    http://127.0.0.1:8080/deleteUser?id=11
    http://127.0.0.1:8080/getUser3?email=jack@gmail.com&name=Elsa
    http://127.0.0.1:8080/getUser2?email=jack@gmail.com&name=Elsa
3.了解CrudRepository與JpaRepository區別，並熟悉CrudRepository提供的方法。
4.由於目前我所撰寫的code，無法找到我在lib_mysql資料庫中，usersBorrowInfo表格的資料，故暫時無法實現join的功能。
參考網站：
    1. CrudRepository與JpaRepository區別： https://www.codenong.com/14014086/
    2. 使用Java Spring Boot操作DB：https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/40186/ 、 https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/427709/


----------------------------------------------------------2021/04/15更新----------------------------------------------------------
1.了解Java Spring接MySQL中，若Java Spring裡的變數名稱有小寫轉大寫的情況(駝形轉換)
Java Spring會把駝形的地方，自動轉成底線，使得MySQL解讀錯誤，因而找不到欄位。(ex:booksName => books_name)

2.使用Map方法，幫助Java Spring完成MySQL之join操作，Map<String,String>，將欄位與欄位資料
對應在一起！(ex:"name":"Amy")

3.了解Java Spring的unit test，目前能使用Junit test，但Maven test偵測不到我要測試的程式區段。


----------------------------------------------------------2021/04/22更新----------------------------------------------------------
1.透過重新建立專案，使用Spring Boot 2.4.5，已能讓Maven test偵測到要測試的程式區段。
2.簡單使用JUnit Test搭配Mockito工具，來完成程式單元測試的實現。
3.了解JPA與hibernate的差異，JPA是一個標準規範，而hibernate是JPA底下的一個ORM框架。
參考網站：
    1. Junit Test結合Mockito運用：https://matthung0807.blogspot.com/2018/03/junitmockito.html
    2. JPA與hibernate的差異：https://ek21.com/news/tech/15086/ 、 https://ithelp.ithome.com.tw/articles/10229808


----------------------------------------------------------2021/04/28統整----------------------------------------------------------
目前已學習Java Spring Boot一段時間，也有了一些小成果，故在此稍微整理一下Java Spring Boot的學習成果！
1.已能使用Java Spring Boot的CrudRepository、JpaRepository，對MySQL資料庫做新增、查詢跟刪除的動作。
2.在Java Spring Boot中，使用下Query的方式，對MySQL資料庫進行不同join的操作。
3.能使用Junit Test、JUnit Test搭配Mockito工具、Maven Test等方式，對Java Spring Boot專案進行單元測試。

附註:學習Java Spring Boot所用到的工具簡介
    1.Eclipse IDE for Java Developers - 2020-12
    2.使用Java Spring Boot對MySQL資料庫進行操作：Spring Boot Version 2.4.4
    3.Java Spring Boot unit test：Spring Boot Version 2.4.5
    4.資料庫建置：使用Docker搭配MySQL Workbench


其他讀過的網站：
1. https://medium.com/learning-from-jhipster/13-%E7%94%9A%E9%BA%BC%E6%98%AF-jdbc-orm-jpa-orm%E6%A1%86%E6%9E%B6-hibernate-c762a8c5e112
2. https://blog.csdn.net/lipinganq/article/details/79167982


目前暫時就寫到這邊，若有錯誤，歡迎指教！
